def aes_encrypt(data0):

    from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
    from cryptography.hazmat.backends import default_backend

    #generate key
    key = b"s9Fe68ez2a7Vwo4d"
    data = bytes(data0, encoding="utf-8")
    cipher = Cipher(algorithms.AES(key), modes.ECB(), backend=default_backend())

    #encrypt the data
    encryptor = cipher.encryptor()
    #size it corretly
    padded_data = data + b"\0" * (16 - len(data) % 16)
    ciphertext = encryptor.update(padded_data) + encryptor.finalize()

    #decrypt the data
    decryptor = cipher.decryptor()
    plaintext = decryptor.update(ciphertext) + decryptor.finalize()
    plaintext2 = plaintext.decode()

    #return plain text
    return [ciphertext, plaintext2]