charset = [" ",
           "a","b","c","d","e","f","g","h","i","j","k","l","m",
           "n","o","p","q","r","s","t","u","v","w","x","y","z",
           "A","B","C","D","E","F","G","H","I","J","K","L","M",
           "N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
           "1","2","3","4","5","6","7","8","9","0","*","-",",",
           "!","'","^","+","%","&","/","(",")","=","?","_",".",
           ">","<","#","[","]","{","}","$","€","£","¥","₺",":",
           ";","|","@"]

def zodiac_cipher(content):
    import random
    #destroy 20% of the text
    for i in range(0, int(len(content)/5)):
        random_index = random.randint(0,len(content)-1)
        random_character = random.randint(0,len(charset)-1)
        content[random_index] = charset[random_character]
    return content

def caesar_cipher(content, key):
    newcontent = []
    #shift the characters with key
    for i in content:
        try:
            newcontent.append(charset[charset.index(i) + key])
        except:
            newcontent.append(i)
    return newcontent

def substitution_cipher(content, charset2):
    newcontent = []
    #switch the tables
    for i in content:
        newcontent.append(charset2[charset.index(i)])
    return newcontent

def tableify(content):
    #4 by 4 table of content
    contentTable = []
    row = []
    for i in range(1,5):
        for j in range(1,5):
            row.append(str(content[(i*4) - j]))
        contentTable.append(row)
        row = []
    return contentTable

def shuffle(verticalTables, shuffleKey2):
    shuffledTables = []
    coinSide = ""
    #shuffle based on evens and odds
    for verticalTable in verticalTables:
        shuffledTable = []
        if shuffleKey2 % 2 == 0:
            shuffledTable.append(verticalTable[0])
            shuffledTable.append(verticalTable[2])
            shuffledTable.append(verticalTable[1])
            shuffledTable.append(verticalTable[3])
            coinSide = "even"
        elif shuffleKey2 % 2 == 1:
            shuffledTable.append(verticalTable[1])
            shuffledTable.append(verticalTable[3])
            shuffledTable.append(verticalTable[0])
            shuffledTable.append(verticalTable[2])
            coinSide = "odd"
        shuffledTables.append(shuffledTable)
    return shuffledTables, coinSide

def split(shuffledTables, coinSide):
    splittedTables = []
    #split based on evens and odds
    for shuffledTable in shuffledTables:
        if coinSide == "even":
            splittedTables.append([shuffledTable[0], shuffledTable[2]])
            splittedTables.append([shuffledTable[1], shuffledTable[3]])
        elif coinSide == "odd":
            splittedTables.append([shuffledTable[1], shuffledTable[3]])
            splittedTables.append([shuffledTable[0], shuffledTable[2]])
    return splittedTables

def shiftUp(splittedTablePart, shuffleKey2):
    newSplittedTablePart = []
    row = []
    for j in range (0,2):
        for i in splittedTablePart[j]:
            try:
                row.append(charset[charset.index(i) + shuffleKey2])
            except:
                row.append(charset[charset.index(i)])
        newSplittedTablePart.append(row)
        row = []
    return newSplittedTablePart

def shiftDown(splittedTablePart, shuffleKey2):
    newSplittedTablePart = []
    row = []
    for j in range (0,2):
        for i in splittedTablePart[j]:
            try:
                row.append(charset[charset.index(i) - shuffleKey2])
            except:
                row.append(charset[charset.index(i)])
        newSplittedTablePart.append(row)
        row = []
    return newSplittedTablePart

def flipTable(contentTable):
    verticalTable = []
    row = []
    for i in range(0,4):
        for j in range(0,4):
            row.append(contentTable[j][i])
        verticalTable.append(row)
        row = []
    return verticalTable

def tableify(content):
    #4 by 4 table of content
    contentTable = []
    row = []
    for i in range(1,5):
        for j in range(1,5):
            row.append(str(content[(i*4) - j]))
        contentTable.append(row)
        row = []
    return contentTable

def generateKey(content):
    shuffleKey = 0
    shuffleKey2 = 0
    #find every character in charset[] and return shuffleKey
    for i in content:
        #check if the character is in valid characters list
        if i in charset:
            shuffleKey += charset.index(i)
        else:
            print("---------------------------------")
            print("You Have Invalid Characters.")
            print("---------------------------------")
            return restart()
    for i in [*str(shuffleKey)]:
        shuffleKey2 += int(i)
    return shuffleKey, shuffleKey2

def testPrint(content, shuffleKey, shuffleKey2, contentTables, verticalTables, shuffledTables, splittedTables, shiftedTables, finalTables, finalMessage):
    print("---------------------------------")
    print("Content: ", content)
    print("---------------------------------")
    print("Shuffled Key: ", shuffleKey)
    print("---------------------------------")
    print("Shuffled Key Summary: ", shuffleKey2)
    print("---------------------------------")
    print("Content Tables: ")
    print("---------------------------------")
    for i in range(0, len(contentTables)):
        for j in range(0,4):
            print(contentTables[i][j])
        print("---------------------------------")
    print("Vertical Tables: ")
    print("---------------------------------")
    for i in range(0, len(verticalTables)):
        for j in range(0,4):
            print(verticalTables[i][j])
        print("---------------------------------")
    print("Shuffled Tables: ")
    print("---------------------------------")
    for i in range(0, len(shuffledTables)):
        for j in range(0,4):
            print(shuffledTables[i][j])
        print("---------------------------------")
    print("Splitted Tables: ")
    print("---------------------------------")
    splittedCounter = 0
    for i in range(0, len(splittedTables)):
        for j in range(0, 2):
            print(splittedTables[i][j])
            splittedCounter += 1
            if splittedCounter % 4 == 0:
                print("---------------------------------")
    print("Shifted Tables: ")
    print("---------------------------------")
    shiftedCounter = 0
    for i in range(0, len(shiftedTables)):
        for j in range(0, 2):
            print(shiftedTables[i][j])
            shiftedCounter += 1
            if shiftedCounter % 4 == 0:
                print("---------------------------------")
    print("Final Message: ", finalMessage)
    print("---------------------------------")

def importError():
    print("---------------------------------")
    print("Import Error.")
    print("---------------------------------")

def restart():
    #import dependencies
    try:
        import time
    except:
        importError()
        return restart()
    time.sleep(2)
    start()

def start():
    #import
    try:
        import math
        import random
        from additional_functions.clear import clear
        from additional_functions.dependency import dependency
    except:
        importError()
        return restart()
    
    #install dependencies
    dependency()

    #clear screen
    clear()

    #print menu & get input
    print("---------------------------------")
    print("Charset: {0}".format(charset))
    print("---------------------------------")
    try:
        userinput0 = input("Input String: ")
    except:
        print("---------------------------------")
        print("Input Error.")
        print("---------------------------------")
        return restart()
    
    #get user data and generate e2ee key
    username1 = "ali"
    username2 = "ahmet"
    combined_key = username1 + username2
    key = 0
    for i in combined_key:
        key += charset.index(i)

    #shuffle charset with e2ee key
    random.seed(key)
    charset2 = charset.copy()
    random.shuffle(charset2)

    #arrayify input
    content = [*userinput0]
    print("---------------------------------")
    #zodiac the input
    print("Zodiac Applied:")
    zodiac_applied = zodiac_cipher(content)
    print(zodiac_applied)
    print("---------------------------------")
    #caesar the input
    print("Caesar Applied:")
    caesar_applied = caesar_cipher(zodiac_applied, 2)
    print(caesar_applied)
    print("---------------------------------")
    print("Charset 2: {0}".format(charset2))
    print("---------------------------------")
    #substitute the input
    print("Substitution Applied:")
    substitution_applied = substitution_cipher(caesar_applied, charset2)
    print(substitution_applied)
    content = substitution_applied
    #get shufflekey based on index summary
    shuffleKey, shuffleKey2 = generateKey(content)
    #get table of characters
    contentList = []
    for i in range(0, math.ceil(len(content)/16)):
        contentToList = ""
        for j in range(0, 16):
            try:
                contentToList += content[i*16+j]
            except IndexError:
                contentToList += " "
        contentList.append(contentToList)
    contentTables = []
    for i in range(0, len(contentList)):
        contentTable = tableify(contentList[i])
        contentTables.append(contentTable)
    #flip the table
    verticalTables = []
    for i in range(0, len(contentTables)):
        verticalTable = flipTable(contentTables[i])
        verticalTables.append(verticalTable)
    #shuffle
    shuffledTables, coinSide = shuffle(verticalTables, shuffleKey2)
    #split
    splittedTables = split(shuffledTables, coinSide)
    #shift
    shiftedTables = []
    shiftCounter = 0
    for splittedTable in splittedTables:
        shiftedTable = []
        if shiftCounter % 2 == 0:
            shiftedTable = shiftUp(splittedTable, shuffleKey2)
        elif shiftCounter % 2 == 1:
            shiftedTable = shiftDown(splittedTable, shuffleKey2)
        shiftCounter += 1
        shiftedTables.append(shiftedTable)
    #reconstruct
    finalTables = []
    for i in shiftedTables:
        finalTables.append(i)
    #reconstruct cipher
    finalMessage = ""
    for i in finalTables:
        for j in i:
            for k in j:
                for l in k:
                    finalMessage = finalMessage + l
    #test
    testPrint(content, shuffleKey, shuffleKey2, contentTables, verticalTables, shuffledTables, splittedTables, shiftedTables, finalTables, finalMessage)

start()