def dependency():
    import os
    #pull latest pip version
    os.system('python3 -m pip install --upgrade pip --user')
    #install rsa
    os.system('python3 -m pip install rsa --user')
    #install pycrypto
    os.system('python3 -m pip install pycrypto --user')
    #install base32hex
    os.system('python3 -m pip install base32hex --user')
    #install cryptography
    os.system('python3 -m pip install cryptography --user')
    #install pyOpenSSL
    os.system('python3 -m pip install pyOpenSSL --user')