def desonly():

    #import dependencies
    try:
        from additional_functions.clear import clear
        from aes import aes_encrypt
    except:
        print("---------------------------------")
        print("Import Error.")
        print("---------------------------------")
        desonly()

    #clear screen
    clear()

    #print menu & get input
    print("---------------------------------")
    print("AES Encrypt - Decrypt")
    print("---------------------------------")
    try:
        userinput0 = input("Input String To Encode: ")
    except:
        print("---------------------------------")
        print("Input Error.")
        print("---------------------------------")
        return desonly()
    
    #encrypt and print
    print("---------------------------------")
    print("Encrypted AES: {0}".format(aes_encrypt(userinput0)[0]))
    print("---------------------------------")
    print("Decrypted AES: {0}".format(aes_encrypt(userinput0)[1]))
    print("---------------------------------")

desonly()
